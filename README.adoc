= MetaJade Examples
:imagesdir: doc/images

== Fetch the dependencies

To get the MetaJade language with Gradle:

[source]
----
./gradlew resolveLanguageLibs
----

== Write a program in MetaJade

Clone the repo:

[source]
----
git clone https://gitlab.com/rbattistini/metajade-examples.git
----

Open the project in MPS, by clicking `File > Open` in the menu bar and then select the cloned repository.

To create a solution:

image:create-solution.png[]

To create a model in the newly created solution:

image:new-model.png[]

Then a wizard asks to set the model name and its properties.

image:set-model-name.png[]

In the Model properties wizard select the `Used Languages` tab and choose to add the MetaJade devkit module.

image:set-model-prop.png[]

image:choose-devkit.png[]

Create a new MetaJade concept, i.e. agent, behaviour, module or ontology.

image:create-agent.png[]

As an aid to the use of the MPS editor refer to https://vimeo.com/810063505[this] video in which both the editing experience and an example implementation of the PingPong multi-agent system in the https://gitlab.com/rbattistini/metajade-examples[metajade-examples] repo is shown.

== Launch the Jade platform and the agents

=== Prerequisites

If a local version of MPS is installed it is necessary to specify the property `mpsHomeDir` to add to the classpath the libraries of MPS needed to execute the generated classes from Gradle. Otherwise, MPS can be downloaded with Gradle:

[source]
----
./gradlew resolveMps
----

In addition, it is necessary to modify the Gradle task to start the agents.

[source, kotlin]
----
include::build.gradle.kts[tag=solution-specific-classpath]
----
<1> Uncomment this instruction and write the name of the solution in which the nodes where created.

=== Run the MAS

To start the platform:

[source]
----
./gradlew startPlatform
----

To launch an agent without command line parameters:

[source]
----
./gradlew startContainer -Pagents=<agentName>:<solutionName.modelName.agentName>
----

To launch an agent with command line parameters:

[source]
----
./gradlew startContainer -Pagents=<agentName>:<solutionName.modelName.agentName>(<comma-separated-list-of-parameters>)
----
