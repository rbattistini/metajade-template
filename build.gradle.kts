import de.itemis.mps.gradle.*
import de.itemis.mps.gradle.downloadJBR.DownloadJbrForPlatform
import java.util.Date

buildscript {
    dependencies {
        classpath("de.itemis.mps:mps-gradle-plugin:1.11.+")
    }

    dependencyLocking { lockAllConfigurations() }
}

plugins {
    base
    java
}

// Declare the ID of the Gitlab Repository in which the package is hosted
val projectId = "45247131"

val dependencyRepositories = listOf("https://artifacts.itemis.cloud/repository/maven-mps", "https://gitlab.com/api/v4/projects/$projectId/packages/maven")

// Dependency versions
val mpsVersion = "2021.2.+"
val metajadeVersion = "2021.2"

configurations {
    val mps by creating
    val languageLibs by creating

    dependencies {
        mps("com.jetbrains:mps:$mpsVersion")
        languageLibs("metajade:metajade.languages:$metajadeVersion")  
    }
}

dependencyLocking { lockAllConfigurations() }

repositories {
    for (repoUrl in dependencyRepositories) {
        maven {
            url = uri(repoUrl)
        }
    }
    mavenCentral()
}

val skipResolveMps = project.hasProperty("mpsHomeDir")
val mpsHomeDir = rootProject.file(project.findProperty("mpsHomeDir") ?: "$buildDir/mps")

val resolveMps = if (skipResolveMps) {
    tasks.register("resolveMps") {
        doLast {
            logger.info("MPS resolution skipped")
            logger.info("MPS home: {}", mpsHomeDir.getAbsolutePath())
        }
    }
} else {
    tasks.register("resolveMps", Copy::class) {
        dependsOn(configurations["mps"])
        from({
            configurations["mps"].resolve().map(::zipTree)
        })
        into(mpsHomeDir)
    }
}

val artifactsDir = file("$buildDir/artifacts")
val dependenciesDir = file("$buildDir/dependencies")

tasks {
    val resolveLanguageLibs by registering(Copy::class) {
        from({ configurations["languageLibs"].resolve().map(::zipTree) })
        into(dependenciesDir)
    }

    val cleanMps by registering(Delete::class) {
        delete(fileTree(projectDir) { include("**/classes_gen/**", "**/source_gen/**", "**/source_gen.caches/**", "tmp/**") })
    }
}

val platformName: String by project
val platformHost: String by project
val containerBaseName: String by project

val iets3Dir = file("$dependenciesDir/org.iets3.opensource")
val metajadeDir = file("$dependenciesDir/pikalab.ds.metajade.languages")

tasks.create<JavaExec>("startPlatform") {
    main = "jade.Boot"
    group = "run"
    classpath = files(file("$metajadeDir/pikalab.ds.metajade/jade-4.6.0.jar"))
    
    args("-gui", "-name", platformName, "-container-name", containerBaseName + "main", "-local-host", platformHost)
}
// tag::solution-specific-classpath[]
tasks.create<JavaExec>("startContainer") {
    main = "jade.Boot"
    group = "run"
    
    classpath = files(
        file("$iets3Dir/org.iets3.core.os/lib/pcollections.jar"),
        file("$iets3Dir/org.iets3.core.os/languages/iets3.core.os/org.iets3.core.expr.simpleTypes.runtime.jar"),
        file("$iets3Dir/org.iets3.core.expr.genjava/lib/functionaljava_1.8-4.8-SNAPSHOT.jar"),
        file("$iets3Dir/org.iets3.core.expr.genjava/languages/org.iets3.core.expr.genjava/org.iets3.core.expr.genjava.base.rt.jar"),
        file("$metajadeDir/pikalab.ds.metajade/jade-4.6.0.jar"),
        file("$metajadeDir/pikalab.ds.metajade/languages/MetaJade/MetaJade.java.runtime.jar"),
        fileTree("$buildDir/mps/lib"),
        // file("solutions/<solution-name>/classes_gen") <1>
    )
    
    args("-container", "-container-name", containerBaseName + System.currentTimeMillis(), "-host", platformHost)
    if (project.hasProperty("agents")) {
        val agents = project.property("agents").toString()
        if (agents.isNotBlank()) {
            args("-agents", agents)
        }
    }
}
// end::solution-specific-classpath[]

